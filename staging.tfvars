region = "ap-southeast-1"
vpc_cidr_block = "10.0.0.0/16"
vpc_public_subnet_cidr_block = "10.0.2.0/24"

vpc_private_subnet_cidr_block = "10.0.3.0/24"

availability_zone = "ap-southeast-1b"

instance_type = "t2.micro"

instance_ami = "ami-082105f875acab993"
tags = {
   Name = "Staging"
 }