# terraform-aws

Terraform AWS project
## Terraform Workspace is used to switch between different environments dev, qa, staging, and prod
## Main.tf is the main terraform file and variables.tf is used to define all common variable names.
## For different environments separate variable file is maintained where values are defined. Checkout dev.tfvars, qa.tfvars, staging.tfvars, and prod.tfvars

## Show workspaces
```
terraform workspace list
```

## Create new workspace
```
   terraform workspace new dev
   terraform workspace new qa
   terraform workspace new staging
   terraform workspace new prod
```
## Switch workspace
```
terraform workspace select dev
```
## Terrafrom Initialize
```
terraform init
```

## Terrafrom Plan
```
terraform plan -out "planfile" -var-file dev.tfvars
```

## Terraform Apply
```
terraform apply planfile
```
## Terraform Destroy
```
terraform destroy -var-file dev.tfvars
```