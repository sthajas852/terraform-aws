

variable "region" {
  #default     = "ap-southeast-1"
  type = string
  description = "Region of the VPC"
}

variable "vpc_cidr_block" {
  #default = "10.0.0.0/16"
  type = string
  description = "CIDR block for the VPC"
}

variable "vpc_public_subnet_cidr_block" {
  #default = "10.0.0.0/24"
  type = string
  description = "Prod VPC public subnet CIDR blocks"
}

variable "vpc_private_subnet_cidr_block" {
  #default = "10.0.1.0/24"
  type = string
  description = "Prod VPC private subnet CIDR blocks"
}

variable "availability_zone" {
  #default = "ap-southeast-1a"
  type = string
  description = "Prod VPC availability zones"
}

variable "instance_type" {
  #default = "t2.micro"
  type = string
  description = "EC2 instance type"
}

variable "instance_ami" {
  #default = "ami-082105f875acab993"
  type = string
  description = "Machine Image"
}

variable "tags" {}
