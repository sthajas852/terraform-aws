region = "ap-southeast-1"
vpc_cidr_block = "10.0.0.0/16"
vpc_public_subnet_cidr_block = "10.0.0.0/24"

vpc_private_subnet_cidr_block = "10.0.1.0/24"

availability_zone = "ap-southeast-1a"

instance_type = "t2.micro"

instance_ami = "ami-082105f875acab993"
tags = {
   Name = "Dev"
 }